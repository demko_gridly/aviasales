/*


 input#name              Имя коллеги
 input#company           Компания
 textarea#whyyou         Почему именно он
 input#email             Почта в попапе
 input#phone             Телефон в попапе

 button.show-order       Кнопка готово, вызывающая попап
 button.show-socials     Кнопка далее, открывающая социалки
 
 form.send-form			 Форма на главной
 form.send-order		 Форма в попапе


 */

jQuery(document).ready(function($) {
	$('[metrics]').on('change', function(e){
	 	sendAction($(this).attr('metrics'));		
	})
	// маска для телефона
	$('[name=phone]').inputmask('+7 (999) 999-99-99', { showMaskOnHover: false });

	// счетчик символов для why
	$('[name=why-you]').on('keyup', function(e){
		var count = $(this).val().length;
		$('.send-form .input-group__label').html(count + '/700 символов');
	});
	
	// показываем попап с условиями
	$('.show-conds').on('click', function(e){
		e.preventDefault();
		sendAction('rules');
		$('.overlay, .conds-popup').addClass('active');
	});

	// показываем попап с картой
	$('.show-map').on('click', function(e){
		e.preventDefault();
		sendAction('show_map');
		sendAction($(this).attr('metrics'));
		var mapId = $(this).data('map');
		$('.overlay, .map-popup[data-map='+mapId+']').addClass('active');
	})

	// закрываем все попапы при клике на оверлей
	$('.overlay, .popup__close').on('click', function(){
		$('.overlay, .popup, .send-form').removeClass('active');
		$('.overlay').removeClass('form');
	});

	// отключаем скролл страницы, если открыт попап
	$( '.overlay, .popup, .conds-popup__content' ).on( 'mousewheel DOMMouseScroll', function ( e ) {
	    var e0 = e.originalEvent,
	        delta = e0.wheelDelta || -e0.detail;

	    this.scrollTop += ( delta < 0 ? 1 : -1 ) * 20;
	    e.preventDefault();
	});

	// включаем кнопку Далее, если почта впорядке
	$('input#email').on('input', function(){
		if ( (isEmail($(this).val())) && ($('#agreed').prop('checked')) ) {
			$('button.show-socials').removeClass('disabled');
		}
		else {
			$('button.show-socials').addClass('disabled');
			$('.order-popup__left').addClass('removeClass');
			$('.order-popup__right').addClass('disabled');
		}
	});

	$('#agreed').on('change', function(){
		if ( (isEmail($('input#email').val())) && ($('#agreed').prop('checked')) ) {
			$('button.show-socials').removeClass('disabled');
		}
		else {
			$('button.show-socials').addClass('disabled');
			$('.order-popup__left').addClass('removeClass');
			$('.order-popup__right').addClass('disabled');
		}
	});

	// обрабатываем форму на главной
	$('form.send-form').on('submit', function(){

		var form = $(this),
			formName = form.find('[name=name]'),
			formCompany = form.find('[name=company]'),
			formWhy = form.find('[name=why-you]'),
			problems = false;

		// Проверяем поля. В случае ошибки не пускаем дальше и подсвечиваем поле
		if ( !isName(formName.val()) ) {
			problems = true;
			formName.parents('.input-group').addClass('error');
		}
		else {
			formName.parents('.input-group').removeClass('error');	
		}

		if ( !isCompany(formCompany.val()) ) {
			problems = true;
			formCompany.parents('.input-group').addClass('error');
		}
		else {
			formCompany.parents('.input-group').removeClass('error');
		}

		if ( isWhy(formWhy.val()) == 'flood' ) {
			formWhy.parents('.input-group').addClass('error--flood');
			problems = true;	
		}

		if ( isWhy(formWhy.val()) == '000' ) {
			formWhy.parents('.input-group').addClass('error--000');
			problems = true;
		}

		if ( isWhy(formWhy.val()) == '700' ) {
			formWhy.parents('.input-group').addClass('error--700');
			problems = true;	
		}

		if ( isWhy(formWhy.val()) === true ) {
			formWhy.parents('.input-group').removeClass('error error--700 error--000 error--flood');	
		}

		var data = {};
		data['why-you'] = formWhy.val();

		if ( !problems ) {

			// проверка на мат
			var csrftoken = getCookie('csrftoken');
			data['csrfmiddlewaretoken'] = csrftoken;
			$.ajax({
				url: '/step_1/',
				type: 'post',
				data: data,
				async: false,
				success: function(response) {
					if (response !== 'mat') {
						sendAction('click_done')
						$('.overlay, .order-popup, .send-form').addClass('active');
						$('.overlay').addClass('form');
						formWhy.parents('.input-group').removeClass('error--mat');
					}
					else {
						formWhy.parents('.input-group').addClass('error--mat');
					}
				}
			});

		}

		return false;
	});

	// обрабатываем форму в попапе
	$( '.send-order' ).on('submit', function(e){

		var form = $(this),
			data = {},
			problems = false,
			button = form.find('button');

		data['name'] = $('[name=name]').val();
		data['company'] = $('[name=company]').val();
		data['why'] = $('[name=why-you]').val();
		data['email'] = form.find('[name=email]').val();
		data['phone'] = form.find('[name=phone]').val();

		if (button.hasClass('disabled')) {
			if ( !isEmail($('input#email').val()) ) {
				$('input#email').parents('.input-group').addClass('error');
			}
			else {
				$('input#agreed').parents('.input-group').addClass('error');	
			}
			problems = true;
		}
		else {
			$('input#email').parents('.input-group').removeClass('error');
			$('input#agreed').parents('.input-group').removeClass('error');
		}

		if ( $('#agreed').prop('checked') == false ) {
			problems = true;
		}

		var csrftoken = getCookie('csrftoken');
		data['csrfmiddlewaretoken'] = csrftoken;
		$.ajax({
			url: '/step_email/',
			type: 'post',
			data: data,
			async: false,
			success: function(response) {
				if ( response == 'email exists' ) {
					$('input#email').parents('.input-group').addClass('error--exists');
					problems = true;
				}
			}
		});

		if (!problems) {
			sendAction('finish', p={email:data['email'],phone:data['phone']});
			if ( !($('.show-socials').hasClass('disabled')) && !($('.order-popup__right').hasClass('disabled')) ) {
				$('.order-popup__right').addClass('animated tada');
				setTimeout(function(){
					$('.order-popup__right').removeClass('animated tada');
				}, 1000);
			}
 			$('.order-popup__left').addClass('disabled');
			$('.order-popup__right').removeClass('disabled');
 		}

		return false;
	});

});

// плавный скролл до якоря
$(function() {
  $('a[href*="#"]:not([href="#"])').click(function() {
  	if ($(this).hasClass('btn--js--action')) {
  		sendAction($(this).attr('metrics'));
  	};
    if (location.pathname.replace(/^\//,'') == this.pathname.replace(/^\//,'') && location.hostname == this.hostname) {
      var target = $(this.hash);
      target = target.length ? target : $('[name=' + this.hash.slice(1) +']');
      if (target.length) {
        $('html, body').animate({
          scrollTop: target.offset().top
        }, 1000);
        return false;
      }
    }
  });
});

// Проверка имени
function isName(name) {
  var regex = /^([а-яА-Яa-zA-Z ]{2,50})+$/;
  return regex.test(name);
}

// Проверка компании
function isCompany(company) {
	if (company.length) {
		return true;
	}
	else {
		return false;
	}
}

// Проверка текста
function isWhy(text) {
	var result = true,
		flood = /(.)\1{4}/;
	if ( text.length == 0 ) {
		return '000';
	}
	if ( text.length > 700 ) {
		return '700';
	}
	if (flood.test(text)) {
		return 'flood';
	}
	return result;
}

// Проверка почты
function isEmail(email) {
  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email);
}

// Манипуляции после успешной отправки заявки
function sendForm(url) {

	var data = {};

	data['name'] = $('[name=name]').val();
	data['company'] = $('[name=company]').val();
	data['why'] = $('[name=why-you]').val();
	data['email'] = $('[name=email]').val();
	data['phone'] = $('[name=phone]').val();
	data['social'] = url;

	// отправка на сервер
	var csrftoken = getCookie('csrftoken');
	data['csrfmiddlewaretoken'] = csrftoken;
	$.ajax({
		url: '/step_2/',
		type: 'post',
		data: data,
		success: function(response) {
			console.log(response)
			$('#send-form .container').html("<h2>Твой рассказ уже у нас, <br> осталось дождаться <span>результатов конкурса</span></h2>");
			$('header a.desktop').after('<div class="header__nav"><a href="#" class="header__nav-email">Мы тебя узнали)</a><a href="/?logout" class="header__nav-logout"></a></div>');
		}
	});

	$('.overlay').addClass('active');
	$('.overlay').addClass('form');

	$('.order-popup').removeClass('active');
	$('.thank-popup').addClass('active');
	$('input, textarea').val('');
	$('.input-group__label').html('0/700 символов');
	$('.order-popup__right').removeClass('disabled animated tada');
	$('.order-popup__right').addClass('disabled');
}

// for social sharings
Share = {

	vkontakte: function(purl, ptitle, pimg, text, share) {
		url  = 'https://vk.com/share.php?';
 		url += 'url='          + encodeURIComponent(purl);
		url += '&title='       + encodeURIComponent(ptitle);
		url += '&description=' + encodeURIComponent(text);
		url += '&image='       + encodeURIComponent(pimg);
		url += '&noparse=true';
		Share.popup(url, share);
	},

	facebook: function(purl, ptitle, pimg, text, share) {
		url  = 'https://facebook.com/sharer.php?s=100';
		url += '&p[title]='     + encodeURIComponent(ptitle);
		url += '&p[summary]='   + encodeURIComponent(text);
		url += '&p[url]='       + encodeURIComponent(purl);
 		Share.popup(url, share);
 	},
	twitter: function(purl, ptitle, share) {
		url  = 'https://twitter.com/share?';
		url += 'text='      + encodeURIComponent(ptitle);
		url += '&url='      + encodeURIComponent(purl);
 		url += '&counturl=' + encodeURIComponent(purl);
 		Share.popup(url, share);
 	},

	popup: function(url, share) {
		sendAction(share);
		var width = 700,
			height = 700,
			r = screen.height / 2 - width / 2,
			o = screen.width / 2 - height / 2;
		var win = window.open(url,'','toolbar=0,status=0,width=700,height=700,top='+r+',left='+o+'');
		var timer = setInterval(function(){
			if(win.closed) {
				clearInterval(timer);
				sendForm(url);
			}
		}, 1000);
	}
};
function sendAction(name, p) {
	b = typeof b !== 'undefined' ? b : {};
	window.ga('currentProject.send', {
    hitType: 'event',
    eventCategory: 'user_behaviour',
    eventAction: name,
    params: p
	});
	window.mamka('send_event', {
	    name: name,
	    meta: p
	});
	yaCounter3791698.reachGoal(name, p)
}
// cookie
function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
